const PubNub = require('pubnub')

module.exports = (fastify, options, next) => {
  const listener = {
    status: (status) => {
      pubnub.removeListener(listener)
      if (status.error === true) {
        const error =
          status.errorData.response ? status.errorData.response.error
            : status.errorData
        next(error)
      } else if (status.category === 'PNConnectedCategory') {
        next()
      }
    }
  }
  const pubnub = new PubNub({
    ssl: true,
    uuid: options.uuid,
    publishKey: options.publishKey,
    subscribeKey: options.subscribeKey
  })
  pubnub.addListener(listener)
  pubnub.subscribe({channels: options.channels})
  fastify.addHook('onClose', (fastify, done) => {
    pubnub.removeListener(listener)
    pubnub.unsubscribeAll()
    pubnub.stop()
    done()
  })
  fastify.decorate('pubnub', pubnub)
}

module.exports[Symbol.for('skip-override')] = true
