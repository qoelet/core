const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const fetch = require('node-fetch')
const mkdirp = require('mkdirp')
const {writeFile} = require('fs')
const {join} = require('path')
const {promisify} = require('util')
const rimraf = require('rimraf')

require('./helpers/setup')(configuration)

test('ACME webroot challenge', async (t) => {
  const webroot = join(
    configuration.acme.webroot,
    configuration.acme.prefix
  )
  const challenge = 'test'
  await promisify(mkdirp)(webroot)
  await promisify(writeFile)(join(webroot, challenge), 'hello world')

  const url = `http://localhost:${configuration.acme.port}` +
    `/.well-known/acme-challenge/${challenge}`
  const response = await fetch(url)
  t.is(response.status, 200)
  const text = await response.text()
  t.is(text, 'hello world')

  await promisify(rimraf)(join(webroot, challenge))
})
