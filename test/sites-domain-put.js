const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {s3cmd} = require('../helpers/s3cmd')
const {deploy} = require('./helpers/deploy')
const {getUserAccessToken, getUserId} = require('./helpers/credentials')
const PubNub = require('pubnub')
const {join} = require('path')
const rimraf = require('rimraf')
const {promisify} = require('util')
const {chmod} = require('fs')
const ms = require('ms')
const {sleep} = require('./helpers/sleep')

const acmesh = configuration.acme.acmesh
const acmejs = join(__dirname, 'helpers/acme.js')

const mongo = require('./helpers/database')(configuration)
require('./helpers/setup')(configuration)

const fixture = {}

test('Setup database fixtures', async (t) => {
  fixture.userId = await getUserId()
  fixture.modified = new Date()
  fixture.domain = 'fixture.example.net'

  await mongo.db.collection('users').deleteMany()
  await mongo.db.collection('users').insertMany([{
    modified: fixture.modified,
    userId: fixture.userId
  }])
  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('configurations').deleteMany()
  await mongo.db.collection('certificates').deleteMany()
})

test('Reset & mock acme.sh', async (t) => {
  configuration.acme.acmesh = acmejs
  await promisify(chmod)(acmejs, '755')
  await promisify(rimraf)(configuration.acme.home)
})

test('Resetting object storage', async (t) => {
  return s3cmd(
    configuration.s3,
    '--recursive',
    '--force',
    'del',
    `s3://${configuration.s3.bucket}`
  )
})

test('Setup client fixtures', async (t) => {
  fixture.files = [
    {
      data: 'hello world',
      filepath: 'index.html'
    },
    {
      data: 'fake code',
      filepath: 'scripts/app.js'
    },
    {
      data: 'dummy',
      filepath: 'unknown_mime_type'
    }
  ]

  fixture.accessToken = await getUserAccessToken()
  const origin = `https://localhost:${configuration.port}`
  fixture.url = `${origin}/v2/sites/${fixture.domain}`
})

test('Deploy a site', async (t) => {
  const expectedDeploy = {
    type: 'site-deploy',
    domain: fixture.domain,
    isNewDomain: true,
    hasNewFiles: true,
    hasNewConfiguration: true
  }

  const subscribeKey = configuration.pubnub.subscribeKey
  const pubnub = new PubNub({subscribeKey})
  pubnub.subscribe({channels: configuration.pubnub.channels})
  const pubnubNotification = new Promise((resolve) => {
    const expectedMessageCount = 2
    let receivedMessageCount = 0
    pubnub.addListener({
      status (status) {
        console.log(status)
      },
      message ({message}) {
        if (message.type === 'site-deploy') {
          t.deepEqual(message, expectedDeploy)
          receivedMessageCount++
        }
        if (message.type === 'certificate-issue') {
          const expectedIssue = {
            type: 'certificate-issue',
            domain: fixture.domain
          }
          t.deepEqual(message, expectedIssue)
          receivedMessageCount++
        }
        if (receivedMessageCount === expectedMessageCount) {
          pubnub.stop()
          resolve()
        }
      }
    })
  })

  const response = await deploy(fixture)

  t.true(response.ok)
  t.deepEqual(await response.json(), expectedDeploy)

  const hostOptions = await mongo.db.collection('configurations')
    .findOne({domain: fixture.domain})
  t.not(hostOptions, null)
  t.is(hostOptions.domain, fixture.domain)
  t.true('configuration' in hostOptions)
  t.true('modified' in hostOptions)

  const listPublic = await s3cmd(
    configuration.s3,
    'ls',
    `s3://${configuration.s3.bucket}/sites/${fixture.domain}/public/`
  )

  t.ok(/public\/index\.html/.test(listPublic.stdout))

  const listCrypto = await s3cmd(
    configuration.s3,
    'ls',
    `s3://${configuration.s3.bucket}/sites/${fixture.domain}/crypto/`
  )

  t.ok(/crypto\/key.pem/.test(listCrypto.stdout))
  t.ok(/crypto\/cert.pem/.test(listCrypto.stdout))

  await pubnubNotification

  const actualCertificate = await mongo.db.collection('certificates')
    .findOne({domain: fixture.domain})
  t.not(actualCertificate, null)
  t.not(Number.isNaN(new Date(actualCertificate.issued).getTime()))
})

test('Re-deploying too soon does not renew certificate', async (t) => {
  const expectedCertificate = await mongo.db.collection('certificates')
    .findOne({domain: fixture.domain})
  await deploy(fixture)
  const actualCertificate = await mongo.db.collection('certificates')
    .findOne({domain: fixture.domain})
  t.equal(
    actualCertificate.issued.getTime(),
    expectedCertificate.issued.getTime()
  )
})

test('Re-deploying after some time renews certificate', async (t) => {
  const expired = new Date(Date.now() - ms('60 days'))
  await mongo.db.collection('certificates').updateOne(
    {domain: fixture.domain},
    {$set: {issued: expired}}
  )
  await deploy(fixture)
  await sleep(1000)
  const actualCertificate = await mongo.db.collection('certificates')
    .findOne({domain: fixture.domain})
  t.notEqual(
    actualCertificate.issued.getTime(),
    expired.getTime()
  )
})

test('Restore acme.sh', async (t) => {
  configuration.acme.acmesh = acmesh
})
