#!/usr/bin/env node

const {join} = require('path')
const server = require('./server')

if (process.argv[2] === undefined) {
  console.error(
    'Specify the path to a configuration file.\n' +
    `See: ${join(__dirname, 'core.conf.example.js')}`
  )
  process.exit(1)
}

const configuration = require(join(process.cwd(), process.argv[2]))
server(configuration)
  .then((fastify) => {
    const port = process.env.PORT || configuration.port
    fastify.listen(port, '::', (error) => {
      if (error) {
        console.error(error)
        process.exit(1)
      }
    })
  })
